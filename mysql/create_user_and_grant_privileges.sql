CREATE USER 'userweb'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT, DELETE, UPDATE, INSERT on test3.* TO 'userweb'@'localhost';
FLUSH privileges;