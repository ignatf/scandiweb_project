<?php

// SKU comparing
function compare_sku($product, $subproduct)
{
  return ($product->SKU == $subproduct->SKU);
}

// check if there is an empty value in the sent variables
function is_there_empty(...$fields)
{
    foreach($fields as $field) {
      if ($field == '') {
        return true;
      }
    }
    return false;
}

// check if variable is numeric and if it is greater than null
function is_valid_number($num)
{
  if (is_numeric($num)) {
    return ($num > 0.0);
  }
  return false;
}

// check if array consist only valid values
function is_valid_numbers(...$nums)
{
  foreach($nums as $num) {
    if (! is_valid_number($num)) {
      return false;
    }
  }
  return true;
}

// display errors in unordered list in html
function error_list($errors)
{
  $err_el = '';
  if (! empty($errors)) {
    $err_el .= "<div class=\"error\">";
    $err_el .= "<h2> Wrong input, check errors! </h2>";
    $err_el .= "<ul>";
    foreach ($errors as $err) {
      $err_el .= "<li> $err </li>";
    }
    $err_el .= "</ul>";
    $err_el .= "</div>";
  }
  return $err_el;
}

// display message in html
function sentMsg($msgText, $type)
{
  $_SESSION['message'] = $msgText;
  echo '<div id="al" class="alert '. $type .'">';
  echo $_SESSION['message'];
  unset($_SESSION['message']);
  echo '</div>';
}
