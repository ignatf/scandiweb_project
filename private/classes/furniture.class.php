<?php

class Furniture extends Product
{

  public $height;
  public $width;
  public $flength;

  // making query (overriding)
  public static function getData()
  {
    $sql = "SELECT product.SKU, product.name, product.price, product.type, ";
    $sql .= "furniture.height, furniture.width, furniture.flength ";
    $sql .= "FROM product, furniture WHERE product.SKU = furniture.SKU";
    return self::passQuery($sql);
  }

  // adding to database (overriding)
  public function addToDatabase()
  {
    parent::addToDatabase();
    $sql = "INSERT INTO furniture VALUES (";
    $sql .= " ?, ?, ?, ? ";
    $sql .= ")";
    $result = Furniture::$databaseConnection->query($sql, $this->SKU, $this->height, $this->width, $this->flength);
    return $result;
  }

  // function that returns special attribute of the specified class
  public function getAdditionalParam()
  {
    return "{$this->makeDimensions()} cm";
  }

  // check for invalid syntax (overriding)
  protected function validation()
  {
    parent::validation();
    if (! is_valid_numbers($this->height, $this->width, $this->flength)) {
      $this->err[] = "All dimensions must be numeric and greater than 0";
    }
  }

  // constructor
  public function __construct($assoc_array=[])
  {
    parent::__construct($assoc_array);
    $this->height = $assoc_array['height'] ?? 0.0;
    $this->width = $assoc_array['width'] ?? 0.0;
    $this->flength = $assoc_array['flength'] ?? 0.0;
  }

  // represent dimensions in HxWxL format
  public function makeDimensions()
  {
    if (! is_there_empty($this->height, $this->width, $this->flength)) {
      $array = array($this->height, $this->width, $this->flength);
      $result = implode("x", $array);
    }
    return $result;
  }

}
