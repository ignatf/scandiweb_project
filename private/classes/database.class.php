<?php
class Database
{
  private static $pdo;
  public $dsn;

//singleton pattern
  public static function getInstance()
  {
    if(self::$pdo != NULL){
      return self::$pdo;
    }
    return new self;
  }

//database constructor (PDO)
  public function __construct()
  {
    $this->dsn = 'mysql:host='. DB_HOST .';dbname='. DB_NAME .';charset=utf8mb4';
    $options = [
      PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];
    try {
      $this->pdo = new PDO($this->dsn, DB_USER, DB_PASSWORD, $options);
    } catch (Exception $e) {
      error_log($e->getMessage());
      exit("Error connecting to database");
    }
  }

//passing query to database
  public function query($sqlQuery, ...$params)
  {
    $stmt = $this->pdo->prepare($sqlQuery);
    $stmt->execute($params);
    $result = $stmt;
    $stmt = null;
    return $result;
  }
}
