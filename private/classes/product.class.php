<?php
class Product
{

  public $SKU;
  public $name;
  public $price;
  public $type;

//-- start of class functions for interacting with database --//
  public static $databaseConnection;
  public $err = [];

  // set database connection
  public static function setDbc(Database $database)
  {
    self::$databaseConnection = $database;
  }

  // creating an array of objects from the result of the sent query
  public static function passQuery($sql)
  {
    $result = self::$databaseConnection->query($sql);
    if(! $result) {
      exit("Database query failed! ");
    }

    $obj_array = [];
    while($row = $result->fetch()) {
      $obj_array[] = static::instantiate($row);
    }

    return $obj_array;
  }

  // making a query
  public static function getData()
  {
    $sql = 'SELECT * FROM product';
    return self::passQuery($sql);
  }

  // making an object
  protected static function instantiate($row)
  {
    $className = get_called_class();
    $object = new $className;
    foreach($row as $property => $value) {
      if(property_exists($object, $property)) {
        $object->$property = $value;
      }
    }
    return $object;
  }

  // check if SKU already exists in the database
  public function isExistSku()
  {
    $sql = 'SELECT COUNT(*) FROM product WHERE SKU = ?';
    $result = self::$databaseConnection->query($sql, $this->SKU);
    return $result->fetchColumn();

  }

  // adding to database
  public function addToDatabase()
  {
    $this->validation();
    if(! empty($this->err)) {return false;}
    $sql = 'INSERT INTO product VALUES (?, ?, ?, ?)';
    $result = self::$databaseConnection->query($sql, $this->SKU, $this->name, $this->price, $this->type);
    return $result;
  }

  // deleting from database
  public function delete()
  {
    $sql = "DELETE p.*, o.* ";
    $sql .= "FROM product p ";
    $sql .= "INNER JOIN ";
    $sql .= " $this->type o ";
    $sql .= "ON p.SKU = o.SKU ";
    $sql .= "WHERE p.SKU = ? ";
    $result = self::$databaseConnection->query($sql, $this->SKU);
    return $result;
  }

//-- end of class function for interacting with database --/

  // constructor
  public function __construct($assoc_array=[])
  {
    $this->SKU = $assoc_array['SKU'] ?? '';
    $this->name = $assoc_array['name'] ?? '';
    $this->price = $assoc_array['price'] ?? 0.0;
    $this->type = $assoc_array['type'] ?? 'none';
  }

  // display additional parameter of specified object
  public function displayAdditionalParam()
  {
    $className = ucfirst($this->type);
    $specProducts = $className::getData();
    foreach ($specProducts as $specProduct) {
      if (compare_sku($this, $specProduct)) {
       return $specProduct->getAdditionalParam();
      }
    }
    return "none";
  }

  // check input for invalid syntax
  protected function validation()
  {
    if(is_there_empty($this->SKU, $this->name, $this->price, $this->type, $this->getAdditionalParam())) {
      $this->err[] = "Please don't leave empty fields!";
    }
    if(! is_valid_number($this->price)) {
      $this->err[] = "Price must be numeric and greater than 0!";
    }
    if($this->isExistSku()) {
      $this->err[] = "SKU value must be unique!";
    }
    return $this->err;
  }
}
