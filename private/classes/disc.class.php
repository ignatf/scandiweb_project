<?php

class Disc extends Product
{

  public $size;

  // making query (overriding)
  public static function getData()
  {
    $sql = "SELECT product.SKU, product.name, product.price, ";
    $sql .= "product.type, disc.size FROM product, ";
    $sql .= "disc WHERE product.SKU = disc.SKU";
    return self::passQuery($sql);
  }

  // adding to database (overriding)
  public function addToDatabase()
  {
    parent::addToDatabase();
    $sql = "INSERT INTO disc VALUES (?, ?)";
    $result = self::$databaseConnection->query($sql, $this->SKU, $this->size);
    return $result;
  }

  // function that returns special attribute of the specified class
  public function getAdditionalParam()
  {
    return "$this->size MB";
  }

  // constructor
  public function __construct($assoc_array=[])
  {
    parent::__construct($assoc_array);
    $this->size = $assoc_array['size'] ?? '';
  }

  // check for invalid syntax (overriding)
  protected function validation()
  {
    parent::validation();
    if (! is_valid_number($this->size)) {
      $this->err[] = "Size must be numeric and greater than 0";
    }
  }
}
