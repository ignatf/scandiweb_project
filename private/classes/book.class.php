<?php

class Book extends Product
{

  public $bweight;

  // making query (overriding)
  public static function getData() {
    $sql = "SELECT product.SKU, product.name, product.price, ";
    $sql .= "product.type, book.bweight ";
    $sql .= "FROM product, book WHERE product.SKU = book.SKU";
    return self::passQuery($sql);
  }

  //adding to database (overriding)
  public function addToDatabase()
  {
    parent::addToDatabase();
    $sql = "INSERT INTO book VALUES (";
    $sql .= " ?, ? ";
    $sql .= ")";
    $result = Book::$databaseConnection->query($sql, $this->SKU, $this->bweight);
    return $result;
  }

  // function that returns special attribute of the specified class
  public function getAdditionalParam()
  {
    return "$this->bweight kg";
  }

  // constructor
  public function __construct($assoc_array=[])
  {
    parent::__construct($assoc_array);
    $this->bweight = $assoc_array['bweight'] ?? 0.0;
  }

  // check for invalid syntax (overriding)
  protected function validation()
  {
    parent::validation();
    if (! is_valid_number($this->bweight)) {
      $this->err[] = "Weight must be numeric and greater than 0";
    }
  }
}
