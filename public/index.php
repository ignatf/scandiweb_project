<?php
  require_once('../private/pdo_connection.php');
   ?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Test Number Three</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<form method="post" action="index.php">
<!-- Start of header -->
  <div class="d-flex flex-column flex-md-row align-items-center p-2 px-md-4">
    <h5 class="my-0 mr-md-auto">Product List</h5>
    <a class="p-2 text-dark mr-5" href="product-add.php">Add product</a>
    <div class="input-group">
      <select id="productListSelect" onchange="getValue(this)">
        <option value="normal">Normal view</option>
        <option value="delete">Mass delete</option>
      </select>
      <div class="input-group-append">
        <button id="deleteBtn" class="btn btn-outline-primary" type="submit" name="delete" disabled>Submit</button>
      </div>
    </div>
  </div>
  <hr class="mt-1">
<!-- End of header -->
<h1>
<?php
Product::setDbc(Database::getInstance());
$products = Product::getData();
?>
</h1>
<div class="container-fluid">
<!-- Start of products output -->
    <div class="row justify-content-center">
      <?php foreach($products as $product) : ?>
        <?php $display = true ?>
        <!-- Start of product delete -->
        <?php
        if(isset($_POST['delete'])) {
          $checkboxes = $_POST['checkbox'];
          foreach($checkboxes as $checkbox) {
            if ($product->SKU == $checkbox) {
              $display = false;
              $product->delete();
            }
          }
        }
        if ($display) :
        ?>
        <!-- End of product delete -->
        <div class="col-sm-6 col-md-3">
          <div class="card mb-3 shadow-sm products">
            <div class="card-body">
              <input type="checkbox" class="card-title cardCheckBox" name="checkbox[]" value="<?php echo $product->SKU  ?>">
              <div class="card-block text-center">
                <h4 class="card-text"><?php echo $product->SKU ?></h4>
                <p class="card-text"><?php echo $product->name ?></p>
                <p class="card-text"><?php echo $product->price ?> $</p>
                <p class="card-text"><?php echo $product->displayAdditionalParam() ?></p>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <?php endforeach; ?>
    </div>
<!-- End of products output -->
</div>
</form>
<script>
  var checkboxEl = document.querySelectorAll('input.card-title.cardCheckBox');
  // function to disable mass delete button
  function disableBtn() {
    document.getElementById('deleteBtn').disabled = true;
  }
  // function to enable mass delete button
  function enableBtn() {
    document.getElementById('deleteBtn').disabled = false;
  }
  // function which display checkboxes if in drop-down list selected mass delete option
  function getValue(sel) {
    if(sel.value == "normal") {
      disableBtn();
      for (var i=0;i<checkboxEl.length;i++) {
        checkboxEl[i].style.display = 'none';
      }
    } else {
      enableBtn();
      for (var i=0;i<checkboxEl.length;i++) {
        checkboxEl[i].style.display = 'block';
      }
    }
  }
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
