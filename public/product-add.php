<?php
  require_once('../private/pdo_connection.php');
 ?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Test Number Three Second Page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<!-- Header start -->
<div class="d-flex flex-column flex-md-row align-items-center p-2 px-md-4">
  <h5 class="my-0 mr-md-auto">Product Add</h5>
  <a class="text-dark p-2 mr-5" href="index.php">Product List</a>
</div>
<hr class="mt-1">
<!-- Header end -->
<?php
if (isset($_POST['addToDatabase'])): ?>
  <?php
  Product::setDbc(Database::getInstance());
  $arr = $_POST['product'];
  $className = $arr['type'];
  $product = new $className($arr);
  $product->addToDatabase();
  if (!empty($product->err)) {
    sentMsg(error_list($product->err), "alert-danger");
  } else {
    sentMsg("Product is added to database", "alert-success");
  }
  ?>
  <script>
    var hideAlert = function() {
      var alertEl = document.getElementById("al");
      alertEl.style.display = 'none';
    }
    window.setTimeout(hideAlert, 10000);
  </script>
<?php endif ?>
<!-- start of input -->
<div class="container-fluid">
<form action="product-add.php" method="post">
  <div class="form-group">
    <label for="inputSKU"> SKU </label>
    <input type="text" class="form-control" name="product[SKU]" id="inputSKU" placeholder="Enter SKU">
  </div>
  <div class="form-group">
    <label for="inputName"> Name </label>
    <input type="text" class="form-control" name="product[name]" id="inputName" placeholder="Enter name of product">
  </div>
  <div class="form-group">
    <label for="inputPrice"> Price </label>
    <input type="text" class="form-control" name="product[price]" id="inputPrice" placeholder="Enter price of product">
  </div>
  <div class=form-group>
    <label for="typeSwitch"> Type Switcher </label>
    <select class="form-control" id="typeSwitch" name="product[type]" onchange="getVal(this)">
      <option value="disc">DVD-disc</option>
      <option value="book">Book</option>
      <option value="furniture">Furniture</option>
    </select>
  </div>
  <div class="form-group tooltip-group" id="dvdDiscForm">
    <label for="inputSize"> Size </label>
    <input type="text" class="form-control" name="product[size]" id="inputSize" placeholder="Enter size of disc">
    <span class="tooltip-text"> Provide size in MB </span>
  </div>
  <div class="form-group tooltip-group" id="bookForm">
    <label for="inputWeight"> Weight </label>
    <input type="text" class="form-control" name="product[bweight]" id="inputWeight" placeholder="Enter book weight">
    <span class="tooltip-text"> Provide weight in kg </span>
  </div>
  <div class="form-group tooltip-group" id="furnitureForm">
    <label for="inputHeight"> Height </label>
    <input type="text" class="form-control" name="product[height]" id="inputHeight" placeholder="Enter furniture height">
    <span class="tooltip-text"> Provide all dimensions in cm </span>
    <label for="inputWidth"> Width </label>
    <input type="text" class="form-control" name="product[width]" id="inputWidth" placeholder="Enter furniture width">
    <label for="inputLength"> Length </label>
    <input type="text" class="form-control" name="product[flength]" id="inputLength" placeholder="Enter furniture length">
  </div>
  <button type="sumbit" class="btn btn-primary" name="addToDatabase">Add</button>
</form>
</div>
<!-- end of input -->
<script>
  var dvdDiscForm = document.getElementById("dvdDiscForm");
  var bookForm = document.getElementById("bookForm");
  var furnitureForm = document.getElementById("furnitureForm");
  bookForm.style.display = 'none';
  furnitureForm.style.display = 'none';
  // function that displays input for selected type of product
  function getVal(val) {
    switch (val.value) {
      case 'disc':
        dvdDiscForm.style.display = 'block';
        bookForm.style.display = 'none';
        furnitureForm.style.display = 'none';
        break;
      case 'book':
        dvdDiscForm.style.display = 'none';
        bookForm.style.display = 'block';
        furnitureForm.style.display = 'none';
        break;
      case 'furniture':
        dvdDiscForm.style.display = 'none';
        bookForm.style.display = 'none';
        furnitureForm.style.display ='block';
        break;
    }
  }
</script>
</body>
</html>
